package com.humanresources.colaborador;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ColaboradorRepository extends JpaRepository<Colaborador, Long> {

    List<Colaborador> findAllByCargo_IdCargo(Long idCargo);
    List<Colaborador> findAllByIdColaboradorIn(List<Long> idColaboradores);

    List<Colaborador> findAllByGestor_IdColaborador(Long idGestor);
}
