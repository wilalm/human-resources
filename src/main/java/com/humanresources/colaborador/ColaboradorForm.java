package com.humanresources.colaborador;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.humanresources.cargo.Cargo;
import com.humanresources.departamento.Departamento;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class ColaboradorForm {

    private String nome;
    private String rg;
    private String cpf;
    private String ctps;
    private LocalDate dataNascimento;
    private BigDecimal salario;
    private Long idGestor;
    private Long idCargo;
    private Long idDepartamento;

//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private LocalDate dataContratacao;

    @JsonIgnore
    private Colaborador gestor;

    @JsonIgnore
    private Cargo cargo;

    @JsonIgnore
    private Departamento departamento;

    public Colaborador transformar(Cargo cargo, Departamento departamento) {
        return Colaborador.builder()
                .nome(this.nome)
                .rg(this.rg)
                .cpf(this.cpf)
                .ctps(this.ctps)
                .dataNascimento(this.dataNascimento)
                .salario(this.salario)
                .gestor(this.gestor)
                .dataContratacao(this.dataContratacao)
                .cargo(cargo)
                .departamento(departamento)
                .build();
    }

}
