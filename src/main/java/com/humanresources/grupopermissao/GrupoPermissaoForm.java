package com.humanresources.grupopermissao;

import lombok.Data;

import java.util.List;

@Data
public class GrupoPermissaoForm {

    private List<Long> idPermissoes;
}
