package com.humanresources.usuario;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.humanresources.grupo.Grupo;
import lombok.Data;

@Data
public class UsuarioForm {

    private String email;
    private String senha;

    private Long idColaborador;
    private Long idGrupo;

    @JsonIgnore
    private Grupo grupo;

    public Usuario transformar() {
        return Usuario.builder()
                .email(this.email)
                .senha(this.senha)
                .build();
    }
}
