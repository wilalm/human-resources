package com.humanresources.planocarreira;

import lombok.Data;

@Data
public class PlanoCarreiraForm {

    private String nome;
    private String descricao;

    public PlanoCarreira transformar() {
        return PlanoCarreira.builder()
                .nome(this.nome)
                .descricao(this.descricao)
                .build();
    }
}
