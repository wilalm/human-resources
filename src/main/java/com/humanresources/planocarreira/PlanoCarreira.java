package com.humanresources.planocarreira;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.humanresources.cargo.Cargo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Builder
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class PlanoCarreira {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idPlanoCarreira;

    private String nome;
    private String descricao;

    @ManyToMany
    @JoinTable(name = "plano_carreira_cargo",
            joinColumns = @JoinColumn(name = "id_plano_carreira"),
            inverseJoinColumns = @JoinColumn(name = "id_cargo"))
    @JsonIgnoreProperties(
            "beneficios"
    )
    private List<Cargo> cargos;

}
