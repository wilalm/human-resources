package com.humanresources.planocarreira;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PlanoCarreiraRepository extends JpaRepository<PlanoCarreira, Long> {
}
