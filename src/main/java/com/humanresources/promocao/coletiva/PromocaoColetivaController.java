package com.humanresources.promocao.coletiva;

import com.humanresources.promocao.PromocaoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Api(tags = "Promoções", description = "Promoções de cargo e salário ou apenas de salário")
@RequestMapping("/promocao-coletiva")
@RestController
public class PromocaoColetivaController {

    @Autowired
    private PromocaoService promocaoService;

    @GetMapping("/cargo/{idCargo}")
    public ResponseEntity listarColaboradoresEmUmCargo(@PathVariable Long idCargo) {
        return ResponseEntity.ok(promocaoService.listarPossiveisPromocoesColetivas(idCargo));
    }

    @ApiOperation(value = "Promover lista de colaboradores")
    @PutMapping("/cargo")
    public ResponseEntity promoverColaboradores(@RequestBody PromocaoColetivaRequest promocaoColetivaRequest) {
        promocaoService.promoverListaDeFuncionarios(promocaoColetivaRequest);
        return ResponseEntity.ok().build();
    }

}
