package com.humanresources.promocao.coletiva;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.humanresources.cargo.Cargo;
import com.humanresources.colaborador.Colaborador;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

@Data
@Builder
public class PromocaoColetivaResponse {

    @JsonIgnoreProperties({
            "gestor", "rg", "ctps", "data_contratacao", "data_nascimento", "salario", "departamento", "cargo", "avaliacoes", "avaliacoes_feitas", "usuario", "eventos"
    })
    private List<Colaborador> colaboradores;

    private BigDecimal salarioMaximoRecomendadoParaOCargoAtual;

    @JsonIgnoreProperties({
            "beneficios"
    })
    private Set<Cargo> cargosIndicados;

    @JsonIgnoreProperties({
            "beneficios"
    })
    private Set<Cargo> cargosPossiveis;
}
