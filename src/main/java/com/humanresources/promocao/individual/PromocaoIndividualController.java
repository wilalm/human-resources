package com.humanresources.promocao.individual;

import com.humanresources.promocao.PromocaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

//@Api(tags = "Promoções Individuais", description = "Promoções individuais de cargo e salário ou apenas de salário")
@RequestMapping("/promocao-individual")
@RestController
public class PromocaoIndividualController {

    @Autowired
    private PromocaoService promocaoService;

//    @GetMapping("/colaborador/{idColaborador}")
//    public ResponseEntity listarPossiveisPromocoesParaUmColaborador(@PathVariable Long idColaborador) {
//        return ResponseEntity.ok(promocaoService.listarPossiveisPromocoes(idColaborador));
//    }
//
    @PutMapping("/colaborador/{idColaborador}")
    public ResponseEntity promoverColaborador(@PathVariable Long idColaborador,
                                              @RequestBody PromocaoIndividualRequest request) {
        request.setIdColaborador(idColaborador);
        promocaoService.promoverFuncionario(request);
        return ResponseEntity.ok().build();
    }
}
