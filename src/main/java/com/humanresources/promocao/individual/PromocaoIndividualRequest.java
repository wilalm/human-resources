package com.humanresources.promocao.individual;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class PromocaoIndividualRequest {

    @JsonIgnore
    private Long idColaborador;

    private BigDecimal novoSalario;

    @JsonProperty("novo_cargo")
    private Long idCargo;
}
