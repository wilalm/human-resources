package com.humanresources.promocao;

import com.humanresources.cargo.Cargo;
import com.humanresources.cargo.CargoService;
import com.humanresources.colaborador.Colaborador;
import com.humanresources.colaborador.ColaboradorService;
import com.humanresources.planocarreiracargo.PlanoCarreiraCargo;
import com.humanresources.planocarreiracargo.PlanoCarreiraCargoService;
import com.humanresources.promocao.coletiva.PromocaoColetivaRequest;
import com.humanresources.promocao.coletiva.PromocaoColetivaResponse;
import com.humanresources.promocao.individual.PromocaoIndividualRequest;
import com.humanresources.promocao.individual.PromocaoIndividualResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class PromocaoService {

    @Autowired
    private ColaboradorService colaboradorService;

    @Autowired
    private PlanoCarreiraCargoService planoCarreiraCargoService;

    @Autowired
    private CargoService cargoService;

    public PromocaoIndividualResponse listarPossiveisPromocoes(Long idColaborador) {
        Colaborador colaborador = colaboradorService.buscarOuFalhar(idColaborador);

        Set<Cargo> proximosCargosIndicados = colaborador.getCargo().getPlanoCarreiraCargo().stream()
                .map(planoCarreiraCargo ->
                        planoCarreiraCargoService.buscarPorIdPlanoCarreiraESequencia(
                                planoCarreiraCargo.getIdPlanoCarreira(), planoCarreiraCargo.getSequencia() + 1)
                                .orElse(null))
                .filter(Objects::nonNull)
                .map(PlanoCarreiraCargo::getCargo)
                .collect(Collectors.toSet());

        Set<Cargo> proximosCargosPossiveis = cargoService.listar().stream()
                .filter(cargo -> !proximosCargosIndicados.contains(cargo))
                .collect(Collectors.toSet());

        return PromocaoIndividualResponse.builder()
                .cargosIndicados(proximosCargosIndicados)
                .cargosPossiveis(proximosCargosPossiveis)
                .salarioMaximoRecomendadoParaOCargoAtual(colaborador.getCargo().getSalarioMaxRecomendado())
                .build();
    }

    @Transactional
    public PromocaoColetivaResponse listarPossiveisPromocoesColetivas(Long idCargo) {
        Cargo cargoAtual = cargoService.buscarOuFalhar(idCargo);
        List<Colaborador> colaboradores = colaboradorService.listarColaboradorComCargo(idCargo);

        Set<Cargo> proximosCargosIndicados = cargoAtual.getPlanoCarreiraCargo().stream()
                .map(planoCarreiraCargo ->
                        planoCarreiraCargoService.buscarPorIdPlanoCarreiraESequencia(
                                planoCarreiraCargo.getIdPlanoCarreira(), planoCarreiraCargo.getSequencia() + 1)
                                .orElse(null))
                .filter(Objects::nonNull)
                .map(PlanoCarreiraCargo::getCargo)
                .collect(Collectors.toSet());

        Set<Cargo> proximosCargosPossiveis = cargoService.listar().stream()
                .filter(cargo -> !proximosCargosIndicados.contains(cargo))
                .collect(Collectors.toSet());

        return PromocaoColetivaResponse.builder()
                .colaboradores(colaboradores)
                .cargosIndicados(proximosCargosIndicados)
                .cargosPossiveis(proximosCargosPossiveis)
                .salarioMaximoRecomendadoParaOCargoAtual(cargoAtual.getSalarioMaxRecomendado())
                .build();
    }

    @Transactional
    public void promoverListaDeFuncionarios(PromocaoColetivaRequest promocaoColetivaRequest) {
        List<Long> idColaboradores = promocaoColetivaRequest.getIdColaboradores();
        List<Colaborador> colaboradores = colaboradorService.listar(idColaboradores);
        Long idCargo = promocaoColetivaRequest.getIdCargo();
        BigDecimal novoSalario = promocaoColetivaRequest.getNovoSalario();

        if (!Objects.isNull(idCargo)) {
            Cargo novoCargo = cargoService.buscarOuFalhar(idCargo);
            colaboradorService.promoverPorCargo(colaboradores, novoCargo, novoSalario);
        } else {
            colaboradorService.promoverPorSalario(colaboradores, novoSalario);
        }
    }

    @Transactional
    public void promoverFuncionario(PromocaoIndividualRequest promocaoIndividualRequest) {
        Long idColaborador = promocaoIndividualRequest.getIdColaborador();
        Colaborador colaborador = colaboradorService.buscarOuFalhar(idColaborador);
        Long idCargo = promocaoIndividualRequest.getIdCargo();
        BigDecimal novoSalario = promocaoIndividualRequest.getNovoSalario();

        if (!Objects.isNull(idCargo)) {
            Cargo novoCargo = cargoService.buscarOuFalhar(idCargo);
            colaboradorService.promoverPorCargo(colaborador, novoCargo, novoSalario);
        } else {
            colaboradorService.promoverPorSalario(colaborador, novoSalario);
        }
    }

}
