package com.humanresources.planocarreiracargo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.humanresources.cargo.Cargo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Builder
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class PlanoCarreiraCargo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long idPlanoCarreiraCargo;

//    @Column(name = "id_cargo")
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_cargo")
    @JsonIgnoreProperties({
            "beneficios", "planoCarreiraCargo"
    })
    private Cargo cargo;

    @JsonIgnore
    @Column(name = "id_plano_carreira")
    private Long idPlanoCarreira;

    private int sequencia;

    @Transient
    @JsonProperty("cargo")
    private CargoDto cargoDto;

    public void completarCargoDto() {
        this.cargoDto = CargoDto.builder()
                .idCargo(cargo.getIdCargo())
                .nome(cargo.getNome())
                .descricao(cargo.getDescricao())
                .salarioMaxRecomendado(cargo.getSalarioMaxRecomendado())
                .salarioMinRecomendado(cargo.getSalarioMinRecomendado())
                .build();
    }
}
