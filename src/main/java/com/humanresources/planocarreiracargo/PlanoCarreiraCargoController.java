package com.humanresources.planocarreiracargo;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Api(tags = "PlanoCarreiraCargo", description = "Associação entre Plano de Carreira e Cargo")
@RestController
@RequestMapping("/plano_carreira/{idPlanoCarreira}/cargos")
public class PlanoCarreiraCargoController {

    @Autowired
    private PlanoCarreiraCargoService planoCarreiraCargoService;

    @GetMapping
    public ResponseEntity listar(@PathVariable Long idPlanoCarreira) {
        return ResponseEntity.ok(planoCarreiraCargoService.listar(idPlanoCarreira));
    }

    @PutMapping
    public ResponseEntity associar(@PathVariable Long idPlanoCarreira,
                                   @RequestBody PlanoCarreiraCargoForm planoCarreiraCargoForm) {
        planoCarreiraCargoService.associar(idPlanoCarreira, planoCarreiraCargoForm);

        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{idCargo}")
    public ResponseEntity desassociar(@PathVariable Long idPlanoCarreira,
                                       @PathVariable Long idCargo) {
        planoCarreiraCargoService.desassociar(idPlanoCarreira, idCargo);

        return ResponseEntity.ok().build();
    }

}
