package com.humanresources.cargo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CargoRepository extends
        JpaRepository<Cargo, Long>,
        JpaSpecificationExecutor<Cargo> {


    List<Cargo> findAllByBeneficios_IdBeneficio(Long idBeneficio);
}
