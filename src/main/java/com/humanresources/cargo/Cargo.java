package com.humanresources.cargo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.humanresources.beneficio.Beneficio;
import com.humanresources.planocarreiracargo.PlanoCarreiraCargo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Builder
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Cargo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idCargo;

    private String nome;
    private String descricao;
    private BigDecimal salarioMinRecomendado;
    private BigDecimal salarioMaxRecomendado;

    @ManyToMany
    @JoinTable(name = "cargo_beneficio",
            joinColumns = @JoinColumn(name = "id_cargo"),
            inverseJoinColumns = @JoinColumn(name = "id_beneficio"))
    private List<Beneficio> beneficios;

    @OneToMany(mappedBy = "cargo")
    @JsonIgnore
    private List<PlanoCarreiraCargo> planoCarreiraCargo;

    public List<Beneficio> beneficiosPerdidos(Cargo cargoAntigo) {
        return cargoAntigo.getBeneficios().stream()
                .filter(beneficio -> !beneficios.contains(beneficio))
                .collect(Collectors.toList());
    }

    public List<Beneficio> beneficiosGanhados(Cargo cargoAntigo) {
        return beneficios.stream()
                .filter(beneficio -> !cargoAntigo.getBeneficios().contains(beneficio))
                .collect(Collectors.toList());

    }

    public void adicionarBeneficio(Beneficio beneficio) {
        getBeneficios().add(beneficio);
    }

    public void removerBenefício(Beneficio beneficio) {
        getBeneficios().remove(beneficio);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cargo cargo = (Cargo) o;
        return idCargo.equals(cargo.idCargo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idCargo);
    }
}
