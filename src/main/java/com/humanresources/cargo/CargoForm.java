package com.humanresources.cargo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class CargoForm {

    private String nome;
    private String descricao;
    private BigDecimal salarioMinRecomendado;
    private BigDecimal salarioMaxRecomendado;

    public Cargo transformar() {
        return Cargo.builder()
                .nome(this.nome)
                .descricao(this.descricao)
                .salarioMinRecomendado(this.salarioMinRecomendado)
                .salarioMaxRecomendado(this.salarioMaxRecomendado)
                .build();
    }
}
