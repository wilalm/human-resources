package com.humanresources.avaliacao;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.humanresources.colaborador.Colaborador;
import lombok.Data;

@Data
public class AvaliacaoForm {

    private float notaComportamento;
    private float notaTecnica;
    private float notaTrabalhoEmEquipe;
    private String comentario;
    private Long idGestor;

    @JsonIgnore
    private Colaborador gestor;

    @JsonIgnore
    private Colaborador colaborador;

    public Avaliacao transformar() {
        return Avaliacao.builder()
                .notaComportamento(this.notaComportamento)
                .notaTecnica(this.notaTecnica)
                .notaTrabalhoEmEquipe(this.notaTrabalhoEmEquipe)
                .comentario(this.comentario)
                .build();
    }
}
