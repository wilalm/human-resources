package com.humanresources.avaliacao;

import com.humanresources.colaborador.ColaboradorService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Api(tags = "Avaliação", description = "CRUD de Avaliação")
@RestController
@RequestMapping("/colaboradores/{idColaborador}/avaliacoes")
public class AvaliacaoController {

    @Autowired
    private ColaboradorService colaboradorService;

    @Autowired
    private AvaliacaoService avaliacaoService;

    @GetMapping
    public ResponseEntity listar(@PathVariable Long idColaborador,
                                 Pageable pageable) {

        colaboradorService.buscarOuFalhar(idColaborador);
        return ResponseEntity.ok(avaliacaoService.listar(idColaborador, pageable));
    }

    @GetMapping("/{idAvaliacao}")
    public ResponseEntity buscar(@PathVariable Long idColaborador,
                                 @PathVariable Long idAvaliacao) {
        return ResponseEntity.ok(avaliacaoService.buscarOuFalhar(idColaborador, idAvaliacao));
    }

    @PostMapping
    public ResponseEntity salvar(//UriComponentsBuilder uriComponentsBuilder,
                                 @PathVariable Long idColaborador,
                                 @RequestBody AvaliacaoForm avaliacaoForm) {

        return ResponseEntity.ok(avaliacaoService.salvar(idColaborador, avaliacaoForm));
    }

    @PutMapping("/{idAvaliacao}")
    public ResponseEntity atualizar(@PathVariable Long idColaborador,
                                    @PathVariable Long idAvaliacao,
                                    @RequestBody AvaliacaoForm avaliacaoForm) {

        return ResponseEntity.ok(avaliacaoService.atualizar(idColaborador, idAvaliacao, avaliacaoForm));
    }

    @DeleteMapping("/{idAvaliacao}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remover(@PathVariable Long idColaborador,
                        @PathVariable Long idAvaliacao) {

        avaliacaoService.remover(idColaborador, idAvaliacao);
    }
}
