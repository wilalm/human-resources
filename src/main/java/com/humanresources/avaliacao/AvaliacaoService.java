package com.humanresources.avaliacao;

import com.humanresources.colaborador.Colaborador;
import com.humanresources.colaborador.ColaboradorService;
import com.humanresources.evento.tipos.InclusaoAvaliacao;
import com.humanresources.exception.exceptions.EntidadeNaoEncontradaException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class AvaliacaoService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private AvaliacaoRepository avaliacaoRepository;

    @Autowired
    private ColaboradorService colaboradorService;

    @Autowired
    private ApplicationEventPublisher publisher;

    public Page<Avaliacao> listar(Long idColaborador,
                                  Pageable pageable) {
        return avaliacaoRepository.findAllByColaboradorIdColaborador(idColaborador, pageable);
    }

    public Avaliacao buscarOuFalhar(Long idColaborador, Long idAvaliacao) {

        colaboradorService.buscarOuFalhar(idColaborador);

        return avaliacaoRepository.findByColaboradorIdColaboradorAndIdAvaliacao(idColaborador, idAvaliacao)
                .orElseThrow(() ->
                        new EntidadeNaoEncontradaException(
                                String.format("Não existe uma avaliação com id %s ou ela não pertence ao colaborador %s", idAvaliacao, idColaborador)));
    }

    public boolean possuiAvaliacaoComoGestor(Colaborador colaborador) {
        return avaliacaoRepository.findAllByGestorIdColaborador(colaborador.getIdColaborador()).isPresent();
    }

    public Avaliacao salvar(Long idColaborador, AvaliacaoForm avaliacaoForm) {

        Colaborador colaborador = colaboradorService.buscarOuFalhar(idColaborador);
        Colaborador gestor = colaboradorService.buscarOuFalhar(avaliacaoForm.getIdGestor());

        Avaliacao novaAvaliacao = avaliacaoForm.transformar();
        novaAvaliacao.setColaborador(colaborador);
        novaAvaliacao.setGestor(gestor);

        colaborador.adicionarAvaliacao(novaAvaliacao);

        publisher.publishEvent(new InclusaoAvaliacao(colaborador));

        return avaliacaoRepository.save(novaAvaliacao);
    }

    public Avaliacao atualizar(Long idColaborador, Long idAvaliacao, AvaliacaoForm avaliacaoForm) {
        Colaborador colaborador = colaboradorService.buscarOuFalhar(idColaborador);

        //TODO
        //PERMITIR MUDANÇA DE GESTOR

        Avaliacao avaliacaoAtual = buscarOuFalhar(colaborador.getIdColaborador(), idAvaliacao);
        avaliacaoForm.setColaborador(colaborador);

        modelMapper.map(avaliacaoForm, avaliacaoAtual);

        return avaliacaoRepository.save(avaliacaoAtual);
    }

    public void remover(Long idColaborador, Long idAvaliacao) {
        Avaliacao avaliacao = buscarOuFalhar(idColaborador, idAvaliacao);

        avaliacaoRepository.delete(avaliacao);
    }
}
