package com.humanresources.avaliacao;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.humanresources.colaborador.Colaborador;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDate;

@Builder
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Avaliacao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idAvaliacao;

    private float notaComportamento;
    private float notaTecnica;
    private float notaTrabalhoEmEquipe;
    private String comentario;

    @CreationTimestamp
    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDate data = LocalDate.now();

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_colaborador")
    private Colaborador colaborador;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_gestor")
    private Colaborador gestor;

    @JsonProperty("nome_gestor")
    public String getNomeGestor() {
        return gestor.getNome();
    }
}
