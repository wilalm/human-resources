package com.humanresources.grupo;

import lombok.Data;

@Data
public class GrupoForm {

    private String nome;

    public Grupo transformar() {
        return Grupo.builder()
                .nome(this.nome)
                .build();
    }
}
