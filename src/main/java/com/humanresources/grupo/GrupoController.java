package com.humanresources.grupo;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Api(tags = "Grupo", description = "CRUD de Grupo")
@RestController
@RequestMapping("/grupos")
public class GrupoController {

    @Autowired
    private GrupoService grupoService;

    @GetMapping
    public ResponseEntity listar(Pageable pageable) {
        return ResponseEntity.ok(grupoService.listar(pageable));
    }

    @GetMapping("/{idGrupo}")
    public ResponseEntity buscar(@PathVariable Long idGrupo) {
        return ResponseEntity.ok(grupoService.buscarOuFalhar(idGrupo));
    }

    @PostMapping
    public ResponseEntity salvar(//UriComponentsBuilder uriComponentsBuilder,
                                 @RequestBody GrupoForm grupoForm) {

        return ResponseEntity.ok(grupoService.salvar(grupoForm));
    }

    @PutMapping("/{idGrupo}")
    public ResponseEntity atualizar(@PathVariable Long idGrupo,
                                    @RequestBody GrupoForm grupoForm) {

        return ResponseEntity.ok(grupoService.atualizar(idGrupo, grupoForm));
    }

    @DeleteMapping("/{idGrupo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remover(@PathVariable Long idGrupo) {

        grupoService.remover(idGrupo);
    }
}
