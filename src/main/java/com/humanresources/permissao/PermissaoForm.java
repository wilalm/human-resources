package com.humanresources.permissao;

import lombok.Data;

@Data
public class PermissaoForm {

    private String nome;
    private String descricao;

    public Permissao transformar() {
        return Permissao.builder()
                .nome(this.nome)
                .descricao(this.descricao)
                .build();
    }
}
