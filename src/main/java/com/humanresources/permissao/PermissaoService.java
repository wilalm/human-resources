package com.humanresources.permissao;

import com.humanresources.exception.exceptions.EntidadeNaoEncontradaException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class PermissaoService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private PermissaoRepository permissaoRepository;

    public Page<Permissao> listar(Pageable pageable) {
        return permissaoRepository.findAll(pageable);
    }

    public Permissao buscarOuFalhar(Long idPermissao) {
        return permissaoRepository.findById(idPermissao)
                .orElseThrow(() ->
                        new EntidadeNaoEncontradaException(
                                String.format("Não existe uma permissão com id %s", idPermissao)));
    }


    public Permissao salvar(PermissaoForm permissaoForm) {

        Permissao novaPermissao = permissaoForm.transformar();

        return permissaoRepository.save(novaPermissao);
    }


    public Permissao atualizar(Long idPermissao, PermissaoForm permissaoForm) {
        Permissao permissaoAtual = buscarOuFalhar(idPermissao);

        modelMapper.map(permissaoForm, permissaoAtual);

        return permissaoRepository.save(permissaoAtual);
    }

    public void remover(Long idPermissao) {
        Permissao Permissao = buscarOuFalhar(idPermissao);
        permissaoRepository.delete(Permissao);
    }
}
