package com.humanresources.beneficio;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class BeneficioForm {

    private String nome;
    private String descricao;
    private BigDecimal valor;
    private Integer coparticipacao;

    public Beneficio transformar() {
        return Beneficio.builder()
                .nome(this.nome)
                .descricao(this.descricao)
                .valor(this.valor)
                .coparticipacao(this.coparticipacao)
                .build();
    }
}
