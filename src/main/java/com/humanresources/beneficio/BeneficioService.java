package com.humanresources.beneficio;

import com.humanresources.cargo.Cargo;
import com.humanresources.cargo.CargoService;
import com.humanresources.colaborador.Colaborador;
import com.humanresources.colaborador.ColaboradorService;
import com.humanresources.evento.tipos.AlteracaoBeneficio;
import com.humanresources.exception.exceptions.EntidadeEmUsoException;
import com.humanresources.exception.exceptions.EntidadeNaoEncontradaException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class BeneficioService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private BeneficioRepository beneficioRepository;

    @Autowired
    private ApplicationEventPublisher publisher;

    @Autowired
    private CargoService cargoService;

    @Autowired
    private ColaboradorService colaboradorService;

    @PersistenceContext
    private EntityManager manager;

    public List<Beneficio> listar() {
        return beneficioRepository.findAll();
    }

    public Page<Beneficio> listar(Pageable pageable) {
        return beneficioRepository.findAll(pageable);
    }

    public Beneficio buscarOuFalhar(Long idBeneficio) {
        return beneficioRepository.findById(idBeneficio)
                .orElseThrow(() ->
                        new EntidadeNaoEncontradaException(
                                String.format("Não existe um benefício com id %s", idBeneficio)));
    }


    public Beneficio salvar(BeneficioForm beneficioForm) {
        if (Objects.isNull(beneficioForm.getCoparticipacao())) {
            beneficioForm.setCoparticipacao(0);
        }
        Beneficio novoBeneficio = beneficioForm.transformar();

        return beneficioRepository.save(novoBeneficio);
    }


    public Beneficio atualizar(Long idBeneficio, BeneficioForm beneficioForm) {
        Beneficio beneficioAtual = buscarOuFalhar(idBeneficio);

        if (Objects.nonNull(beneficioForm.getValor()) || Objects.nonNull(beneficioForm.getCoparticipacao())) {
            Beneficio beneficioParaOEvento = beneficioAtual;
            manager.detach(beneficioParaOEvento);
            List<Cargo> cargos = cargoService.listaCargosQuePossuemOBeneficio(beneficioParaOEvento.getIdBeneficio());
            List<Colaborador> colaboradores = cargos
                    .stream()
                    .map(cargo -> colaboradorService.listarColaboradorComCargo(cargo.getIdCargo()))
                    .flatMap(Collection::stream)
                    .collect(Collectors.toList());

            if (Objects.isNull(beneficioForm.getCoparticipacao())) {
                beneficioForm.setCoparticipacao(beneficioAtual.getCoparticipacao());
            }

            publisher.publishEvent(new AlteracaoBeneficio(beneficioParaOEvento, beneficioAtual.getValor(), beneficioForm.getValor(),
                    beneficioAtual.getCoparticipacao(), beneficioForm.getCoparticipacao(), colaboradores));
        }
        modelMapper.map(beneficioForm, beneficioAtual);
        return beneficioRepository.save(beneficioAtual);
    }

    public void remover(Long idBeneficio) {

        try {
            Beneficio beneficio = buscarOuFalhar(idBeneficio);

            beneficioRepository.delete(beneficio);
        } catch (DataIntegrityViolationException e) {
            throw new EntidadeEmUsoException(String.format("O benefício %s não pode ser excluído pois há cargos que o possuem", idBeneficio));
        }
    }
}
