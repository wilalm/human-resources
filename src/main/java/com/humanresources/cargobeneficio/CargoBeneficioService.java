package com.humanresources.cargobeneficio;

import com.humanresources.beneficio.Beneficio;
import com.humanresources.beneficio.BeneficioService;
import com.humanresources.cargo.Cargo;
import com.humanresources.cargo.CargoService;
import com.humanresources.colaborador.Colaborador;
import com.humanresources.colaborador.ColaboradorService;
import com.humanresources.evento.tipos.InclusaoBeneficio;
import com.humanresources.evento.tipos.RemocaoBeneficio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CargoBeneficioService {

    @Autowired
    private CargoService cargoService;

    @Autowired
    private BeneficioService beneficioService;

    @Autowired
    private ColaboradorService colaboradorService;

    @Autowired
    private ApplicationEventPublisher publisher;

    public List<Beneficio> listarAssociaveis(Long idCargo) {
        List<Beneficio> beneficiosDoCargo = listar(idCargo);
        List<Beneficio> beneficiosCadastrados = beneficioService.listar();

        return beneficiosCadastrados.stream()
                .filter(beneficio -> !beneficiosDoCargo.contains(beneficio))
                .collect(Collectors.toList());
    }

    public List<Beneficio> listar(Long idCargo) {
        Cargo cargo = cargoService.buscarOuFalhar(idCargo);

        return cargo.getBeneficios();
    }

    @Transactional
    public void associar(Long idCargo, Long idBeneficio) {
        Cargo cargo = cargoService.buscarOuFalhar(idCargo);
        Beneficio beneficio = beneficioService.buscarOuFalhar(idBeneficio);

        List<Colaborador> colaboradores = colaboradorService.listarColaboradorComCargo(cargo.getIdCargo());
        publisher.publishEvent(new InclusaoBeneficio(beneficio, colaboradores));

        cargo.adicionarBeneficio(beneficio);
    }

    @Transactional
    public void desassociar(Long idCargo, Long idBeneficio) {
        Cargo cargo = cargoService.buscarOuFalhar(idCargo);
        Beneficio beneficio = beneficioService.buscarOuFalhar(idBeneficio);

        List<Colaborador> colaboradores = colaboradorService.listarColaboradorComCargo(cargo.getIdCargo());
        publisher.publishEvent(new RemocaoBeneficio(beneficio, colaboradores));

        cargo.removerBenefício(beneficio);
    }
}
