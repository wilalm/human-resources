package com.humanresources.cargobeneficio;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Api(tags = "CargoBenefício", description = "Associação entre Cargo e Benefício")
@RestController
@RequestMapping("/cargos/{idCargo}/beneficios")
public class CargoBeneficioController {

    @Autowired
    private CargoBeneficioService cargoBeneficioService;

    @GetMapping
    public ResponseEntity listar(@PathVariable Long idCargo,
                                 @RequestParam(required = false) boolean associaveis) {
        if (associaveis) {
            return ResponseEntity.ok(cargoBeneficioService.listarAssociaveis(idCargo));
        }
        return ResponseEntity.ok(cargoBeneficioService.listar(idCargo));
    }

    @PutMapping("/{idBeneficio}")
    public ResponseEntity associar(@PathVariable Long idCargo,
                                   @PathVariable Long idBeneficio) {
        cargoBeneficioService.associar(idCargo, idBeneficio);

        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{idBeneficio}")
    public ResponseEntity desassociar(@PathVariable Long idCargo,
                                      @PathVariable Long idBeneficio) {
        cargoBeneficioService.desassociar(idCargo, idBeneficio);

        return ResponseEntity.ok().build();
    }
}
