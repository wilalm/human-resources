package com.humanresources.departamento;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Api(tags = "Departamento", description = "CRUD de Departamento")
@RestController
@RequestMapping("/departamentos")
public class DepartamentoController {

    @Autowired
    private DepartamentoService departamentoService;

    @GetMapping
    public ResponseEntity listar(Pageable pageable) {
        return ResponseEntity.ok(departamentoService.listar(pageable));
    }

    @GetMapping("/{idDepartamento}")
    public ResponseEntity buscar(@PathVariable Long idDepartamento) {
        return ResponseEntity.ok(departamentoService.buscarOuFalhar(idDepartamento));
    }

    @PostMapping
    public ResponseEntity salvar(//UriComponentsBuilder uriComponentsBuilder,
                                 @RequestBody DepartamentoForm departamentoForm) {

        return ResponseEntity.ok(departamentoService.salvar(departamentoForm));
    }

    @PutMapping("/{idDepartamento}")
    public ResponseEntity atualizar(@PathVariable Long idDepartamento,
                                    @RequestBody DepartamentoForm departamentoForm) {

        return ResponseEntity.ok(departamentoService.atualizar(idDepartamento, departamentoForm));
    }

    @DeleteMapping("/{idDepartamento}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remover(@PathVariable Long idDepartamento) {

        departamentoService.remover(idDepartamento);
    }
}
