package com.humanresources.departamento;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.humanresources.colaborador.Colaborador;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Builder
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Departamento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idDepartamento;

    private String nome;
    private String descricao;
    private String localizacao;

    @JsonIgnore
    @OneToMany(mappedBy = "departamento")
    private List<Colaborador> colaboradores;
}
