package com.humanresources.relatorio.diversidade_idade;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DiversidadeIdadeResponse {

    private List<DiversidadeIdadeDepartamentoResponse> diversidadeIdadeDepartamentoResponse;

    private LocalDate dataInicio;
    private LocalDate dataFim;

    private double quantidadeTotalColaboradores;
    HashMap<String, Integer> colaboradoresPeriodo = new HashMap<>();
    HashMap<String, Integer> colaboradoresTotal = new HashMap<>();
}
