package com.humanresources.relatorio.diversidade_idade;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class DiversidadeIdadeDepartamentoResponse {

    private String departamento;

    private Integer quantidadeBoomer;
    private Integer quantidadeX;
    private Integer quantidadeY;
    private Integer quantidadeZ;
}
