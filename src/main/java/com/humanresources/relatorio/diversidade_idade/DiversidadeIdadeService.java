package com.humanresources.relatorio.diversidade_idade;

import net.sf.jasperreports.engine.JRException;

public interface DiversidadeIdadeService {

    byte[] emitirRelatorio(DiversidadeIdadeResponse response) throws JRException;
}
