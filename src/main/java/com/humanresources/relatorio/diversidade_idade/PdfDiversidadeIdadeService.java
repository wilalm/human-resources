package com.humanresources.relatorio.diversidade_idade;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Locale;

@Service
public class PdfDiversidadeIdadeService implements DiversidadeIdadeService {

    @Override
    public byte[] emitirRelatorio(DiversidadeIdadeResponse response) throws JRException {
//        try {

        var inputStream = this.getClass().getResourceAsStream(
                "/relatorios/diversidade-idade.jasper");

        var parametros = new HashMap<String, Object>();
        parametros.put("REPORT_LOCALE", new Locale("pt", "BR"));
        parametros.put("data", LocalDate.now());
        parametros.put("data_inicio", response.getDataInicio());
        parametros.put("data_fim", response.getDataFim());

        HashMap<String, Integer> colaboradoresPeriodo = response.getColaboradoresPeriodo();
        parametros.put("boomerUltimoAno", colaboradoresPeriodo.get("Baby Boomer"));
        parametros.put("xUltimoAno", colaboradoresPeriodo.get("Geração X"));
        parametros.put("yUltimoAno", colaboradoresPeriodo.get("Geração Y"));
        parametros.put("zUltimoAno", colaboradoresPeriodo.get("Geração Z"));

        HashMap<String, Integer> colaboradoresTotal = response.getColaboradoresTotal();
        double quantidadeTotalColaboradores = response.getQuantidadeTotalColaboradores();

        parametros.put("totalBoomer", (colaboradoresTotal.get("Baby Boomer") / quantidadeTotalColaboradores) * 100);
        parametros.put("totalX", (colaboradoresTotal.get("Geração X") / quantidadeTotalColaboradores) * 100);
        parametros.put("totalY", (colaboradoresTotal.get("Geração Y") / quantidadeTotalColaboradores) * 100);
        parametros.put("totalZ", (colaboradoresTotal.get("Geração Z") / quantidadeTotalColaboradores) * 100);

        parametros.put("totalContratadoPeriodo", 10);


        var dataSource = new JRBeanCollectionDataSource(response.getDiversidadeIdadeDepartamentoResponse());
        var jasperPrint = JasperFillManager.fillReport(inputStream, parametros, dataSource);

        return JasperExportManager.exportReportToPdf(jasperPrint);
//        } catch (Exception e) {
//            System.out.println("e.getMessage() = " + e.getMessage());
//            System.out.println("entrou");
//            return new byte[0];
//        }
    }
}
