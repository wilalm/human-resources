package com.humanresources.relatorio.eventos_colaborador;

import net.sf.jasperreports.engine.JRException;

public interface EventosColaboradorService {

    byte[] emitirRelatorio(EventosColaboradorResponse response) throws JRException;
}
