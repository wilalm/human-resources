package com.humanresources.evento.tipos;

import com.humanresources.beneficio.Beneficio;
import com.humanresources.colaborador.Colaborador;
import com.humanresources.evento.Event;
import com.humanresources.evento.Evento;
import com.humanresources.evento.TipoEvento;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
public class InclusaoBeneficio implements Event {

    private static final TipoEvento evento = TipoEvento.INCLUSAO_BENEFICIO;
    private static final LocalDateTime data = LocalDateTime.now();

    private Beneficio beneficio;
    private List<Colaborador> colaboradores;

    public List<Evento> toEvento() {
        return colaboradores.stream()
                .map(colaborador ->
                        Evento.builder()
                                .tipoEvento(evento)
                                .data(data)
                                .colaborador(colaborador)
                                .descricao(
                                        String.format(
                                                evento.getMensagem(),
                                                beneficio.getNome(),
                                                beneficio.getValor(),
                                                beneficio.getCoparticipacao()))
                                .build())
                .collect(Collectors.toList());
    }
}
