package com.humanresources.evento.tipos;

import com.humanresources.colaborador.Colaborador;
import com.humanresources.evento.Event;
import com.humanresources.evento.Evento;
import com.humanresources.evento.TipoEvento;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

@Data
@AllArgsConstructor
public class InclusaoAvaliacao implements Event {

    private static final TipoEvento evento = TipoEvento.INCLUSAO_AVALIACAO;
    private static final LocalDateTime data = LocalDateTime.now();

    private Colaborador colaborador;

    public List<Evento> toEvento() {
        return Collections.singletonList(Evento.builder()
                .tipoEvento(evento)
                .data(data)
                .colaborador(colaborador)
                .descricao(evento.getMensagem())
                .build());
    }
}
