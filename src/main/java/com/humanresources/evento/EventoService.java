package com.humanresources.evento;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EventoService {

    @Autowired
    private EventoRepository eventoRepository;

    public void save(List<Evento> eventos) {
//        System.out.println("evento = " + eventos.get(0).getDescricao());
        eventoRepository.saveAll(eventos);
    }
}
