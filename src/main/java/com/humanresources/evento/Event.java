package com.humanresources.evento;

import java.util.List;

public interface Event {

    List<Evento> toEvento();
}
